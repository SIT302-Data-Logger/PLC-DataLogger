import React from 'react';
import './button.css';

const Button = ({
  children,
  onClick,
  disabled,
  loading,
  type = 'button',
}: Props) => (
  <button className="button" onClick={onClick} disabled={disabled} type={type}>
    {children}
    {loading && (
      <i
        className={'fa fa-spinner fa-lg fa-spin'}
        aria-hidden="true"
        style={{ marginLeft: 'auto' }}
      />
    )}
  </button>
);

export default Button;
