import React from 'react';

export default Component => props => {
  return props.loading ? <div>loading...</div> : <Component {...props} />;
};
