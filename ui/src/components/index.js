import withError from './withError';
import withLoader from './withLoader';

export { withError, withLoader };
