import React from 'react';

export default message => Component => props =>
  props.error ? <div>{message}</div> : <Component {...props} />;
