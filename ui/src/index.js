import React from 'react';
import ReactDOM from 'react-dom';
import Rx from 'rxjs';
import { setObservableConfig } from 'recompose';
import App from './app';

import 'whatwg-fetch';

import './index.css';
import 'react-select/dist/react-select.css';

setObservableConfig({
  fromESObservable: Rx.Observable.from,
});

ReactDOM.render(<App />, document.getElementById('root'));
