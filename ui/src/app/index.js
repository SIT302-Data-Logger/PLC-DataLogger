import React from 'react'
import {
  BrowserRouter as Router,
  Route,
  Redirect,
  Link,
  Switch
} from 'react-router-dom'
import cn from 'classnames'
import Log from '../assets/log-clip-art-log-300px.png'
import SourcesHome from '../sources/home'
import RealtimeHome from '../realtime'
import './app.css'

const MenuLink = ({ label, to, activeOnlyWhenExact }) => (
  <Route
    path={to}
    exact={activeOnlyWhenExact}
    children={({ match }) => (
      <div>
        <Link to={to} className={cn('menu-link', { active: match })}>
          {label}
        </Link>
      </div>
    )}
  />
)

const Menu = () => (
  <div className='menu'>
    <ul>
      <li>
        <MenuLink label='Sources' to='/sources' activeOnlyWhenExact />
      </li>
      <li>
        <MenuLink label='Realtime' to='/realtime' />
      </li>
    </ul>
  </div>
)

const Layout = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={matchProps => (
      <div className='app'>
        <header className='app-header'>
          <h1 className='app-title'>Datalogger UI</h1>
          <Menu />
          <img src={Log} alt='log logger log' width={32} height={16} />{' '}
        </header>
        <Component {...matchProps} />
      </div>
    )}
  />
)

const Home = () => <Redirect to='/sources' />

const App = () => (
  <Router>
    <Switch>
      <Layout exact path='/' component={Home} />
      <Layout path='/sources' component={SourcesHome} />
      <Layout path='/realtime' component={RealtimeHome} />
    </Switch>
  </Router>
)

export default App
