import { create_fetch } from '.';

// captialized for object punning
export default async (source, DriverName, Address) =>
  create_fetch({
    url: `/api/sources/${source}`,
    method: 'POST',
    body: {
      DriverName,
      Address,
    },
  });
