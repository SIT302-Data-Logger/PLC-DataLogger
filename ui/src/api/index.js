import fetch_sources from './fetch_sources';
import create_source from './create_source';
import delete_source from './delete_source';

import create_tag from './create_tag';
import delete_tag from './delete_tag';
import fetch_tags from './fetch_tags';

import fetch_drivers from './fetch_drivers';

const headers = {
  'Content-Type': 'application/json',
};

const create_fetch = ({ url, method, body }) => {
  switch (method.toUpperCase()) {
    case 'POST':
      return fetch(url, {
        method,
        headers,
        body: JSON.stringify(body),
      }).then(res => res.json());

    case 'GET':
      return fetch(url, {
        method,
        headers,
      }).then(res => res.json());

    case 'DELETE':
      return fetch(url, {
        method,
        headers,
      }).then(res => res.json());

    default:
      return fetch(url, {
        method: 'GET',
        headers,
      }).then(res => res.json());
  }
};

export {
  /* factory */
  create_fetch,
  /* drivers */
  fetch_drivers,
  /* sources */
  fetch_sources,
  create_source,
  delete_source,
  /* tags */
  fetch_tags,
  create_tag,
  delete_tag,
};
