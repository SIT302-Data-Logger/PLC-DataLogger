import { create_fetch } from '.';

export default async () =>
  create_fetch({
    url: `/api/sources`,
    method: 'GET',
  });
