import { create_fetch } from '.';

export default async source =>
  create_fetch({
    url: `/api/sources/${source}`,
    method: 'DELETE',
  });
