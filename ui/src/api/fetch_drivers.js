import { create_fetch } from '.';

export default async () =>
  create_fetch({
    url: `/api/drivers`,
    method: 'GET',
  });
