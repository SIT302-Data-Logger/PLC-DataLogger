import { create_fetch } from '.';

export default async (source, tag_name) =>
  create_fetch({
    url: `/api/${source}/${tag_name}`,
    method: 'DELETE',
  });
