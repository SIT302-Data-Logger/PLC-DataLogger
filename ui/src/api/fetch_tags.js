import { create_fetch } from '.';

export default async source =>
  create_fetch({
    url: `/api/sources/${source}`,
    method: 'GET',
  }).then(({ tags }) => {
    // FIXME: extract or fix in API
    const tagKeys = Object.keys(tags);
    const sourceTags = tagKeys.map(key => ({
      tag_name: key,
      tag_mode: tags[key].mode,
      tag_interval: tags[key].interval,
    }));
    return sourceTags;
  });
