import { create_fetch } from '.';

export default async (source, tag_name, mode, tag_interval) =>
  create_fetch({
    url: `/api/${source}/${tag_name}`,
    method: 'POST',
    body: {
      Mode: mode,
      Interval: tag_interval,
    },
  }).then(data => {
    const k = Object.keys(data)[0];
    const res = {
      tag_name: k,
      tag_mode: data[k].mode,
      tag_interval: data[k].interval,
    };
    return res;
  });
