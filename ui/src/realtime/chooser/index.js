import React from 'react'
import Select from 'react-select'
import './chooser.css'

const mapValues = sources => sources.map(x => ({ label: x, value: x }))
const mapTagNames = tags =>
  tags.map(({ tag_name: x }) => ({ label: x, value: x }))

class SourceChooser extends React.Component {
  state = {
    selectedOption: ''
  }

  handleChange = selectedOption => {
    this.setState({ selectedOption })
    this.props.onSourceChosen(selectedOption ? selectedOption.label : undefined)
  }

  render () {
    const { selectedOption } = this.state

    return (
      <div className='select-container'>
        <span className='select-label'>Source:</span>
        <Select
          placeholder='Select an available source...'
          noResultsText='No sources available'
          searchable={false}
          options={mapValues(this.props.sources)}
          disabled={!this.props.sources.length}
          className='chooser'
          value={selectedOption}
          onChange={this.handleChange}
        />
      </div>
    )
  }
}

// TODO: make this multiselect
class TagChooser extends React.Component {
  state = {
    selectedOption: ''
  }

  handleChange = selectedOption => {
    this.setState({ selectedOption })
    this.props.onTagChosen(selectedOption ? selectedOption.label : undefined)
  }

  render () {
    const { selectedOption } = this.state

    return (
      <div className='select-container'>
        <span className='select-label'>Tag:</span>
        <Select
          placeholder='(Optional) Select a tag...'
          noResultsText='No tags available'
          searchable={false}
          options={mapTagNames(this.props.tags)}
          disabled={
            !this.props.tags.length ||
              this.props.tagsLoadingState === 'loading' ||
              this.props.tagsLoadingState === 'initial'
          }
          className='chooser'
          value={selectedOption}
          onChange={this.handleChange}
        />
      </div>
    )
  }
}

export { SourceChooser, TagChooser }
