import React from 'react'
import Chart from './chart'
import Viewer from './viewer'
import { SourceChooser, TagChooser } from './chooser'
import { fetch_sources, fetch_tags } from '../api'
import { connect } from './stream'

const State = {
  Initial: 'initial',
  Loading: 'loading',
  Loaded: 'loaded'
}

class Realtime extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      sourcesState: State.Initial,
      tagsState: State.Initial,
      sources: [],
      tags: [],
      selectedSource: undefined,
      selectedTag: '#'
    }

    this.client = connect()
  }

  async componentDidMount () {
    this.setState(() => ({ sourcesState: State.Loading }))
    const sources = await fetch_sources()

    this.setState(state => ({
      ...state,
      sources: [...state.sources, ...sources]
    }))

    const tags = await Promise.all(sources.map(source => fetch_tags(source)))

    this.setState(() => ({ sourcesState: State.Loaded }))
  }

  sourceChosen = async source => {
    this.setState(() => ({ tagsState: State.Loading, selectedSource: source }))
    const tags = await fetch_tags(source)
    this.setState(() => ({ tagsState: State.Loaded, tags: tags || [] }))
  }

  tagChosen = async tag => {
    this.setState(() => ({ selectedTag: tag }))
    // this.setState(() => ({ tagsState: State.Loading }));
    // const tags = await fetch_tags(source);
    // this.setState(() => ({ tagsState: State.Loaded, tags: tags }));
  }

  render () {
    const {
      sources,
      tags,
      sourcesState,
      tagsState,
      selectedSource,
      selectedTag = '#'
    } = this.state

    if (sourcesState === State.Loaded && !Object.keys(sources).length) {
      return <div className='chart-container'>No sources available</div>
    }

    if (sourcesState === State.Loading) {
      return <div className='chart-container'>Loading sources...</div>
    }

    return (
      <div>
        <div
          style={{
            display: 'flex',
            justifyContent: 'center',
            marginTop: '10px'
          }}
        >
          <SourceChooser sources={sources} onSourceChosen={this.sourceChosen} />
          <TagChooser
            tags={tags}
            tagsLoadingState={tagsState}
            onTagChosen={this.tagChosen}
          />
        </div>
        <Chart
          source={selectedSource}
          client={this.client}
          tag={selectedTag}
          allTags={tags}
        />
        <Viewer
          client={this.client}
          source={selectedSource}
          tag={selectedTag}
        />
      </div>
    )
  }
}

export default Realtime
