import Rx from 'rxjs'
import mqtt from 'mqtt'
import map from 'lodash.map'

const POINT_MAX = 40
const LOG_ENTRY_MAX = 100

const connect = () => mqtt.connect(`mqtt://${window.location.host}/api/ws`)

const buildStream = (client, source, tag) => {
  if (client && client.connected) {
    client.subscribe(`Source/${source}/#`, { qos: 2 })
  }

  const socket$ = Rx.Observable.fromEvent(
    client,
    'message',
    (topic, message) => ({ topic, message: message.toString() })
  )

  // message: [{ tag: String, value: Number, time: String }]
  const streamData$ = socket$
    .map(({ message }) => ({ message: JSON.parse(message) }))
    .map(({ message }) => message)
    .scan((acc, val) => acc.concat(val), [])
    .map(values => {
      if (values.length > LOG_ENTRY_MAX) values.shift()
      return values
    })
    .startWith([])

  return streamData$
}

// charts
const buildLogStream = (client, source, tag = '') => {
  if (client.connected) {
    client.subscribe(`Source/${source}/#`, { qos: 2 })
  }

  const socket$ = Rx.Observable.fromEvent(
    client,
    'message',
    (topic, message) => ({ topic, message: message.toString() })
  )

  const streamData$ = socket$
    .map(({ message }) => ({ message: JSON.parse(message) }))
    .map(({ message }) => message)
    .map(({ tag, value, time }) => ({
      tag,
      value,
      time: new Date(time).toISOString()
    }))
    .scan((acc, { tag, value, time }) => {
      const n = {
        ...acc,
        [tag]: {
          name: tag,
          data: acc[tag]
            ? acc[tag].data.concat({
              tag,
              value,
              time
            })
            : [
              {
                tag,
                value,
                time
              }
            ]
        }
      }
      return n
    }, {})
    // restrict each tag's data to max entry count
    .map(values => {
      return map(values, ({ name, data }) => {
        if (data.length > POINT_MAX) data.shift()
        return {
          name,
          data: [...data]
        }
      })
    })
  // .do(_ => console.log(_))

  return streamData$
}

// log viewer
const buildMessageStream = (client, source, tag) => {
  if (client.connected) {
    client.subscribe(`Source/${source}/${tag}`, { qos: 2 })
  }

  const socket$ = Rx.Observable.fromEvent(
    client,
    'message',
    (topic, message) => ({ topic, message: message.toString() })
  )

  const initialValue = {
    tag: '#',
    time: new Date(),
    value: 'Waiting for first message...'
  }

  const messageStream$ = socket$
    .map(({ message }) => ({ message: JSON.parse(message) }))
    .map(({ message }) => message)
    .map(({ tag, value, time }) => ({ tag, value, time: new Date(time) }))
    .scan((acc, { tag, value, time }) => {
      return acc.concat({ tag, value, time })
    }, [])
    .map(values => {
      if (values.length > LOG_ENTRY_MAX) values.shift()
      return values
    })
    .startWith([initialValue])

  return messageStream$
}

export { connect, buildStream, buildLogStream, buildMessageStream }
