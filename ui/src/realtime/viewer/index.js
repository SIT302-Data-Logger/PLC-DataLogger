import React from 'react'
import Rx from 'rxjs'
import { componentFromStream } from 'recompose'
import { buildMessageStream } from '../stream'

import './viewer.css'

class ViewerIndex extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      subscribed: false,
      stream$: Rx.Observable.empty(),
      source: undefined
    }

    this.scrollRef = React.createRef()
  }

  static getDerivedStateFromProps (nextProps, prevState) {
    const { client, source, tag } = nextProps
    const { subscribed } = prevState
    if (source === prevState.source && tag === prevState.tag) return prevState

    const prevSubscription = `Source/${prevState.source}/${prevState.tag}`
    if (subscribed) {
      client.unsubscribe(prevSubscription)
    }

    return {
      ...prevState,
      subscribed: true,
      stream$: buildMessageStream(client, source, tag),
      source,
      tag
    }
  }

  componentWillUnmount () {
    const { subscribed, source, client } = this.state
    if (subscribed && client && client.connected) {
      client.unsubscribe(`Source/${source}/#`)
    }
  }

  // TODO: on chart point hover, highlight the selected log row
  render () {
    const { source, tag, stream$ } = this.state

    if (this.scrollRef.current) {
      this.scrollRef.current.scrollTop = this.scrollRef.current.scrollHeight
    }

    const StreamingLog = componentFromStream(props$ => {
      return props$.combineLatest(stream$, (props, messages) => {
        const el = this.scrollRef.current
        if (el) {
          // if it's scrolled to bottom
          const isAtBottom =
            el.scrollHeight - Math.round(el.scrollTop) === el.clientHeight
          if (isAtBottom) {
            el.scrollTop = el.scrollHeight
          }
        }
        return (
          <div className='viewer-outer'>
            <div className='viewer-scrollcontainer' ref={this.scrollRef}>
              {messages.map(({ time, tag, value }, i) => (
                <div key={i}>
                  <span style={{ display: 'inline-block', width: '150px' }}>
                    [{tag}]
                  </span>
                  {time.toISOString()}:{'\t\t'}{value}
                </div>
              ))}
            </div>
          </div>
        )
      })
    })

    return <StreamingLog source={source} />
  }
}

export default ViewerIndex
