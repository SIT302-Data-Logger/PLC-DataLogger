import React from 'react'
import Rx from 'rxjs'
import map from 'lodash.map'
import { componentFromStream } from 'recompose'
import {
  LineChart,
  Line,
  Legend,
  CartesianGrid,
  XAxis,
  YAxis,
  Tooltip,
  ResponsiveContainer
} from 'recharts'
import { buildLogStream } from '../stream'
import '../realtime.css'
import './chart.css'

const formatDateTime = value => {
  return ''
}

// use the index of the Line (from the map fn) as the dividend
// use the length of the colors array as the divisor
// use mod operator to get a color index
// this way we repeat the colors
const colors = [
  `rgba(128, 230, 198, 0.75)`,
  `rgba(20, 13, 220, 0.75)`,
  `rgba(120, 13, 100, 0.75)`,
  `rgba(234, 128, 92, 0.75)`,
  `rgba(0, 100, 110, 0.75)`
]

const Chart = ({ data, source, tag, allTags }) => {
  const lineName = source ? `${source}/${tag || '#'}` : ''
  return (
    <ResponsiveContainer height={200} width={'100%'}>
      <LineChart margin={{ top: 5, right: 30, left: 20, bottom: 5 }}>
        <XAxis dataKey='time' scale='utcTime' tickFormatter={formatDateTime} />
        <YAxis />
        <CartesianGrid strokeDasharray='3 3' stroke='#39393e' />
        <Tooltip />
        {map(data, ({ name, data: values }, i) => {
          return (
            <Line
              key={i}
              isAnimationActive={false}
              name={name}
              type='monotone'
              data={values}
              dataKey='value'
              stroke={colors[i % colors.length]}
              activeDot={{ r: 4 }}
            />
          )
        })}
        {lineName && <Legend verticalAlign='top' height={36} />}
      </LineChart>
    </ResponsiveContainer>
  )
}

class ChartIndex extends React.Component {
  constructor (props) {
    super(props)
    this.streamData$ = Rx.Observable.empty()
  }

  componentDidMount () {
    const { client, source } = this.props
    this.streamData$ = buildLogStream(client, source)
  }

  componentWillUnmount () {
    const { client } = this.props
    client.unsubscribe(`Source/${this.props.source}/#`)
  }

  render () {
    const { source, tag, allTags } = this.props

    const StreamingChart = componentFromStream(props$ => {
      return props$.combineLatest(this.streamData$, ({ source, tag }, data) => {
        return <Chart data={data} source={source} tag={tag} allTags={allTags} />
      })
    })

    return (
      <div className='outer'>
        <div className='chart-container'>
          <StreamingChart source={source} tag={tag} />
        </div>
      </div>
    )
  }
}

export default ChartIndex

// const List = () => (
//   <ul>
//     {Object.keys(sources).map((source, i) => (
//       <li key={i}>
//         <div>{source}</div>
//         <ul>
//           {Object.keys(sources[source].tags).map((tag, i) => (
//             <li key={i}>{tag}</li>
//           ))}
//         </ul>
//       </li>
//     ))}
//   </ul>
// );
