import React from 'react';
import cn from 'classnames';
import filter from 'lodash.filter';
import { CSSTransition } from 'react-transition-group';
import { Form, TagPills } from '../SourceTags';
import { fetch_tags, create_tag, delete_tag } from '../../api';
import './source-display.css';
import '../SourceTags/tag-pills.css';

class SourceItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      toggled: false,
      tags: [],
      errors: [],
      tagAdding: false,
      tagsFetching: false,
      initialState: true,
    };
  }

  handleClose = () => {
    this.setClosed();
  };

  handleClick = event => {
    event.preventDefault();

    const { toggled } = this.state;

    if (!toggled) {
      this.setState(state => ({ toggled: !state.toggled }));
    }
  };

  setOpen = () => {
    this.setState(state => ({ toggled: true }));
  };

  setClosed = () => {
    this.setState(state => ({ toggled: false }));
  };

  fetchTags = () => {
    this.setState(() => ({ tagsLoading: true }));
    const source = encodeURIComponent(this.props.source);
    setTimeout(async () => {
      const sourceTags = await fetch_tags(source);
      if (this.state.initialState) {
        this.setState(() => ({ initialState: false }));
      }
      this.setState(state => ({
        ...state,
        tags: [...state.tags, ...sourceTags],
        tagsLoading: false,
      }));
    }, 500);
  };

  addTag = async ({ tag_mode: { value: mode }, tag_name, tag_interval }) => {
    const { source } = this.props;
    try {
      const tag = await create_tag(source, tag_name, mode, tag_interval);
      this.setState(state => ({
        ...state,
        tags: [...state.tags, tag],
        tagAdding: false,
      }));
    } catch (e) {
      console.log(`Failed to POST: ${e}`);
      this.setState(state => ({
        ...state,
        errors: [e],
        tagAdding: false,
      }));
    }
  };

  handleRemoveTag = async (source, { tagName }) => {
    try {
      await delete_tag(source, tagName);
      this.setState(state => ({
        ...state,
        tags: filter(state.tags, ({ tag_name }) => tag_name !== tagName),
      }));
    } catch (e) {}
  };

  componentDidMount() {
    this.fetchTags();
  }

  render() {
    const { source, onRemove } = this.props;
    const { toggled, tags } = this.state;

    const itemCss = cn('source-item', { toggled });
    const detailsCss = cn('source-details', { toggled });
    const addTagCss = cn('pill', 'add-tag-button', { toggled });
    return (
      <li className={itemCss}>
        <div style={{ width: '100%' }}>
          <div className={detailsCss}>
            <span>Source Name: {source}</span>
            <div className="pills-list">
              <span>
                <TagPills
                  tags={tags}
                  onRemove={this.handleRemoveTag.bind(null, source)}
                />
              </span>
              <span className={addTagCss} onClick={this.handleClick}>
                Add Tag
              </span>
            </div>

            <CSSTransition
              in={toggled}
              timeout={300}
              classNames="add-tag"
              unmountOnExit
            >
              <div className="form-container">
                <Form onCancel={this.handleClose} onSubmit={this.addTag} />
              </div>
            </CSSTransition>
          </div>
        </div>
        <div className="trash" onClick={onRemove.bind(null, source)} title="Delete source">
          <i className="fa fa-trash-o" aria-hidden="true" />
        </div>
      </li>
    );
  }
}

export default function SourceDisplay({
  sources = [],
  loading,
  initialLoad,
  onRemove,
}) {
  if (initialLoad && loading) {
    return (
      <div>
        <i className={'fa fa-spinner fa-lg fa-spin'} aria-hidden="true" />
      </div>
    );
  }

  return sources.length ? (
    <ul className="source-list">
      {sources.map((source, index) => (
        <SourceItem
          source={source}
          key={index}
          onRemove={onRemove.bind(source)}
        />
      ))}
    </ul>
  ) : (
    <div>No sources exist yet</div>
  );
}
