import React from 'react'
import { withFormik } from 'formik'
import Select from 'react-select'
import ReactTooltip from 'react-tooltip'
import './form.css'
import Button from '../../components/ui/Button'
import { sourceSchema } from '../../schemas'

class DriverSelect extends React.Component {
  handleChange = value => {
    // this is going to call setFieldValue and manually update values.topcis
    this.props.onChange('driver', value)
  }

  handleBlur = () => {
    // this is going to call setFieldTouched and manually update touched.topcis
    this.props.onBlur('driver', true)
  }

  render () {
    const { error, touched } = this.props
    return (
      <div
        className='form-item'
        data-tip=''
        data-for='driver'
        data-event={(!!error && touched && 'focus') || ''}
        data-event-off={(!!error && touched && 'focusout') || ''}
      >
        <label htmlFor='driver' className='required text'>
          Driver:{' '}
        </label>
        <Select
          id='driver'
          placeholder='Select an available driver...'
          noResultsText='No drivers available'
          options={this.props.options}
          onChange={this.handleChange}
          onBlur={this.handleBlur}
          value={this.props.value}
          className={error && touched ? 'invalid' : ''}
        />
        <ReactTooltip
          id='driver'
          place='top'
          type='error'
          effect='solid'
          getContent={() => error}
        />
      </div>
    )
  }
}

const AddSourceForm = props => {
  const {
    values,
    touched,
    errors,
    dirty,
    isValid,
    isSubmitting,
    handleChange,
    handleBlur,
    handleSubmit,
    handleReset,
    setFieldValue,
    setFieldTouched,
    loading,
    drivers
  } = props
  const driversOptions = drivers.map(d => ({ label: d, value: d }))
  return (
    <form onSubmit={handleSubmit}>
      <div className='form-item'>
        <label htmlFor='source_name' className='required text'>
          Source Name:
        </label>
        <input
          id='source_name'
          placeholder='Source Name'
          type='text'
          value={values.source_name}
          onChange={handleChange}
          onBlur={handleBlur}
          className={
            errors.source_name && touched.source_name
              ? 'text-input error'
              : 'text-input'
          }
          data-tip=''
          data-for='sname'
          data-event={
            (errors.source_name && touched.source_name && 'focus') || ''
          }
          data-event-off={
            (errors.source_name && touched.source_name && 'focusout') || ''
          }
        />
      </div>
      {errors.source_name &&
        (touched.source_name &&
          <ReactTooltip
            id='sname'
            place='top'
            type='error'
            effect='solid'
            getContent={() => errors.source_name}
          />)}

      <DriverSelect
        options={driversOptions}
        value={values.driver}
        onChange={setFieldValue}
        onBlur={setFieldTouched}
        error={errors.driver}
        touched={touched.driver}
      />

      <div className='form-item'>
        <label htmlFor='source_address' className='text'>Source Address:</label>
        <input
          id='source_address'
          placeholder='IP Address or hostname'
          type='text'
          value={values.source_address}
          onChange={handleChange}
          onBlur={handleBlur}
          className={
            errors.source_address && touched.source_address
              ? 'text-input error'
              : 'text-input'
          }
          data-tip=''
          data-event={
            (errors.source_address && touched.source_address && 'focus') || ''
          }
          data-event-off={
            (errors.source_address && touched.source_address && 'focusout') ||
              ''
          }
          data-for='address'
        />
        {errors.source_address &&
          (touched.source_address &&
            <ReactTooltip
              id='interval'
              place='top'
              type='error'
              effect='solid'
              getContent={() => errors.source_address}
            />)}
      </div>

      <div className='button-container'>
        <Button onClick={handleReset} disabled={!dirty || isSubmitting}>
          Clear
        </Button>
        <Button type='submit' disabled={isSubmitting || loading || !isValid}>
          Submit
          {loading &&
            <i className={'fa fa-spinner fa-sm fa-spin'} aria-hidden='true' />}
        </Button>
      </div>
    </form>
  )
}

export default withFormik({
  // TODO: use this for editing
  mapPropsToValues: () => ({
    driver: '',
    source_name: '',
    source_address: ''
  }),
  validationSchema: sourceSchema,
  handleSubmit: (values, { props, setSubmitting, resetForm }) => {
    props.onSubmit(values)
    resetForm()
    setSubmitting(false)
  },
  displayName: 'AddSourceForm'
})(AddSourceForm)
