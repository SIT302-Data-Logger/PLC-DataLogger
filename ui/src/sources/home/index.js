import React from 'react';
import filter from 'lodash.filter';
import SourceDisplay from '../SourceDisplay';
import AddSourceForm from '../AddSource/form';
import {
  fetch_drivers,
  fetch_sources,
  create_source,
  delete_source,
} from '../../api';
import './home.css';

const ErrorDisplay = ({ error }) => {
  return <div style={{ color: 'red' }}>{error.message}</div>;
};

class Home extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      sources: [],
      drivers: [],
      errors: [],
      initialState: true,
      sourcesLoading: false,
      driversLoading: false,
      sourceAdding: false,
    };
  }

  componentDidMount() {
    this.getSources();
  }

  async getSources() {
    this.setState(() => ({ sourcesLoading: true, driversLoading: true }));

    // fetch all sources
    const sources = await fetch_sources();
    if (this.state.initialState) {
      this.setState(() => ({ initialState: false }));
    }
    this.setState(state => ({
      ...state,
      sources: [...state.sources, ...sources],
      sourcesLoading: false,
    }));

    // fetch all drivers
    const drivers = await fetch_drivers();
    if (this.state.initialState) {
      this.setState(() => ({ initialState: false }));
    }
    this.setState(state => ({
      ...state,
      drivers: [...state.drivers, ...drivers],
      driversLoading: false,
    }));
  }

  addSource = async ({
    driver: { value: driver_name },
    source_name,
    source_address,
  }) => {
    this.setState(() => ({ sourceAdding: true }));
    try {
      const source = await create_source(
        source_name,
        driver_name,
        source_address
      );
      this.setState(state => ({
        ...state,
        sources: [...state.sources, source],
        sourceAdding: false,
      }));
    } catch (e) {
      console.log(`Failed to POST: ${e}`);
      this.setState(state => ({
        ...state,
        errors: [e],
        sourceAdding: false,
      }));
    }
  };

  deleteSource = async source => {
    try {
      await delete_source(source);
      this.setState(state => ({
        ...state,
        sources: filter(state.sources, s => s !== source),
      }));
    } catch (e) {
      console.log(`Failed to DELETE: ${e}`);
      this.setState(state => ({
        ...state,
        errors: [e],
      }));
    }
  };

  render() {
    const {
      sources,
      drivers,
      errors,
      sourcesLoading,
      // driversLoading,
      initialState,
      sourceAdding,
    } = this.state;
    return (
      <div className="content">
        <h2 className="header">Add Source</h2>
        <div className="section">
          <AddSourceForm
            onSubmit={this.addSource}
            loading={sourceAdding}
            drivers={drivers}
          />
          {errors &&
            errors.map((error, index) => (
              <ErrorDisplay error={error} key={index} />
            ))}
        </div>
        <div style={{ height: '40px' }} />
        <h2 className="header">Sources</h2> 
        <div className="section">
          <SourceDisplay
            sources={sources}
            loading={sourcesLoading}
            initialLoad={initialState}
            onRemove={this.deleteSource}
          />
        </div>
      </div>
    );
  }
}

export default Home;
