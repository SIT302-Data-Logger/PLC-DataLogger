import React from 'react';
import './tag-pills.css';

const Remove = ({ onRemove, tagName }) => (
  <span
    className="pill-delete"
    title="Remove tag from source"
    onClick={() => onRemove({ tagName })}
  >
    <i className="fa fa-trash-o" aria-hidden="true" />
  </span>
);

const TagPill = ({ tagName, onRemove }) => {
  return (
    <li className="pill">
      {tagName} <Remove onRemove={onRemove} tagName={tagName} />
    </li>
  );
};

export default function TagPills({ tags = [], loading = false, onRemove }) {
  if (loading) {
    return (
      <i className={'loading fa fa-spinner fa-lg fa-spin'} aria-hidden="true" />
    );
  }

  if (!tags.length) {
    return null;
  }

  return (
    <ul className="tags-list">
      {tags.map((tag, index) => (
        <TagPill tagName={tag.tag_name} key={index} onRemove={onRemove} />
      ))}
    </ul>
  );
}
