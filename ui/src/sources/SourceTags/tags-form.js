import React from 'react';
import { withFormik } from 'formik';
import Select from 'react-select';
import ReactTooltip from 'react-tooltip';
import Button from '../../components/ui/Button';
import { tagSchema } from '../../schemas';
import '../AddSource/form.css';

const modeOptions = [
  { label: 'Continuous', value: 'continuous' },
  { label: 'Event', value: 'event' },
];

class ModeSelect extends React.Component {
  handleChange = value => {
    // this is going to call setFieldValue and manually update values.topcis
    this.props.onChange('tag_mode', value);
  };

  handleBlur = () => {
    // this is going to call setFieldTouched and manually update touched.topcis
    this.props.onBlur('tag_mode', true);
  };

  render() {
    const { error, touched } = this.props;
    return (
      <div
        className="form-item"
        data-tip=""
        data-for="mode"
        data-event={(!!error && touched && 'focus') || ''}
        data-event-off={(!!error && touched && 'focusout') || ''}
      >
        <label htmlFor="tag_mode" className="required">
          Tag Mode:{' '}
        </label>
        <Select
          id="tag_mode"
          placeholder="Select a tag mode..."
          options={this.props.options}
          onChange={this.handleChange}
          onBlur={this.handleBlur}
          value={this.props.value}
          className={error && touched ? 'invalid' : ''}
        />
        <ReactTooltip
          id="mode"
          place="top"
          type="error"
          effect="solid"
          getContent={() => error}
        />
      </div>
    );
  }
}

const AddTagForm = props => {
  const {
    values,
    touched,
    errors,
    dirty,
    isValid,
    isSubmitting,
    handleChange,
    handleBlur,
    handleSubmit,
    handleReset,
    setFieldValue,
    setFieldTouched,
    loading,
    onCancel,
  } = props;

  return (
    <form onSubmit={handleSubmit}>
      <div>
        <div className="form-item">
          <label htmlFor="tag_name" className="required">
            Tag Name:
          </label>
          <input
            id="tag_name"
            placeholder="Tag Name"
            type="text"
            value={values.tag_name}
            onChange={handleChange}
            onBlur={handleBlur}
            className={
              errors.tag_name && touched.tag_name
                ? 'text-input error'
                : 'text-input'
            }
            data-tip=""
            data-event={(errors.tag_name && touched.tag_name && 'focus') || ''}
            data-event-off={
              (errors.tag_name && touched.tag_name && 'focusout') || ''
            }
            data-for="tname"
          />
          {errors.tag_name &&
            (touched.tag_name && (
              <ReactTooltip
                id="tname"
                place="top"
                type="error"
                effect="solid"
                getContent={() => errors.tag_name}
              />
            ))}
        </div>

        <ModeSelect
          options={modeOptions}
          value={values.tag_mode}
          onChange={setFieldValue}
          onBlur={setFieldTouched}
          error={errors.tag_mode}
          touched={touched.tag_mode}
        />

        <div className="form-item">
          <label htmlFor="tag_interval" className="required">
            Interval:
          </label>
          <input
            id="tag_interval"
            placeholder="Tag Interval (ms)"
            type="number"
            value={values.tag_interval}
            min="100"
            onChange={handleChange}
            onBlur={handleBlur}
            className={
              errors.tag_interval && touched.tag_interval
                ? 'text-input error'
                : 'text-input'
            }
            data-tip=""
            data-event={
              (errors.tag_interval && touched.tag_interval && 'focus') || ''
            }
            data-event-off={
              (errors.tag_interval && touched.tag_interval && 'focusout') || ''
            }
            data-for="interval"
          />
          {errors.tag_interval &&
            (touched.tag_interval && (
              <ReactTooltip
                id="interval"
                place="top"
                type="error"
                effect="solid"
                getContent={() => errors.tag_interval}
              />
            ))}
        </div>

        <div className="button-container">
          <Button onClick={onCancel}>Cancel</Button>
          <Button onClick={handleReset} disabled={!dirty || isSubmitting}>
            Clear
          </Button>
          <Button type="submit" disabled={isSubmitting || loading || !isValid}>
            Add
            {loading && (
              <i className={'fa fa-spinner fa-sm fa-spin'} aria-hidden="true" />
            )}
          </Button>
        </div>
      </div>
    </form>
  );
};

export default withFormik({
  mapPropsToValues: () => ({
    tag_name: '',
    tag_mode: '',
    tag_interval: '',
  }),
  validationSchema: tagSchema,
  handleSubmit: (values, { props, setSubmitting, resetForm }) => {
    props.onSubmit(values);
    resetForm();
    setSubmitting(false);
  },
  displayName: 'AddTagForm',
})(AddTagForm);
