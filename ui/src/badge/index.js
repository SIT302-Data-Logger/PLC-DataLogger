// accepts numSources as prop
import React from 'react';
import './badge.css';

// TODO: loading state
const Badge = ({ numSources }) => (
 <div className='badge'>{numSources}</div>
);

export default Badge;
