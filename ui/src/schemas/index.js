import yup from 'yup';

const sourceSchema = yup.object().shape({
  driver: yup.string().required(),
  source_name: yup
    .string()
    .required()
    .min(2)
    .max(64),
  source_address: yup.string(),
});

const tagSchema = yup.object().shape({
  tag_name: yup.string().required(),
  tag_mode: yup.string().required(),
  tag_interval: yup
    .number()
    .min(100)
    .max(Number.MAX_SAFE_INTEGER)
    .required(),
});

export { sourceSchema, tagSchema };
