#! /usr/bin/python3
#Interface class for dsd Processes
#Daniel McCarthy 2018

import subprocess, json, time, os
#This class use used to instantiate, keep track of and control Driver instances.

class Driver():
    message = None

    def __init__(self, Addr, Name, driverName, mqttclient):
        self.Addr = Addr
        self.driverName = driverName
        self.ClientName = Name
        self.dataChannel = "Source/" + Name
        self.controlChannel = "Control/" + Name
        self.feedbackChannel = "Feedback/" + Name
        self.process = subprocess.Popen(["/usr/src/app/SourceDrivers/{0}".format(self.driverName), self.Addr, self.dataChannel, self.controlChannel, self.feedbackChannel, self.ClientName])
        self.client = mqttclient
        self.client.subscribe(self.feedbackChannel)
        self.client.message_callback_add(self.feedbackChannel, self.MQTTCallback)
        #time.sleep(0.2) #Wait for process to start

    def MQTTCallback(self, rclient, userdata, message):
        self.message = message.payload #(╯°□°）╯︵ ┻━┻ This seems to be the only way...
        #print(message.payload)

    def Start(self): #returns subprocess handle #TODO Modify for non python drivers
        if self.process.poll() == 0:
                print(os.listdir())
                self.process = subprocess.Popen(["/usr/src/app/SourceDrivers/{0}".format(self.driverName), self.Addr, self.dataChannel, self.controlChannel, self.feedbackChannel, self.ClientName])

    def Kill(self): #Kills process
        self.process.kill()
        self.process.wait() #Wait for process to go away

    def SetTag(self, name, interval, mode):
        msg = {"type":"SetTag", "interval":interval, "tag":name, "mode":mode}
        self.client.publish(self.controlChannel, json.dumps(msg), 1) #QoS mode 1  (deliver at least once)

    def DelTag(self, name):
        msg = {"type":"DelTag", "tag":name}
        self.client.publish(self.controlChannel, json.dumps(msg), 1)

    def GetStatus(self):
        msg = {"type" : "GetStatus"}
        self.client.publish(self.controlChannel, json.dumps(msg), 1)
        #Wait to get response from callback (╯°□°）╯︵ ┻━┻
        while self.message == None:
            time.sleep(0.10)
        message = self.message
        self.message = None
        msg_decoded = json.loads(message.decode('utf-8'))
        return msg_decoded

#Tests
if __name__ == "__main__":
    import paho.mqtt.client as mqttlib
    client = mqttlib.Client("Manager")
    client.connect("127.0.0.1")
    client.loop_start()
    Test = Driver("127.0.0.1", "Test", "drv-dummy.py", client)
    Test.AddTag("Test", 10,"constant")
    time.sleep(2)
    print("Status: {0}".format(Test.GetStatus()))
    Test.DelTag("Test")
    time.sleep(1)
    Test.Kill()
