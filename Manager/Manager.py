#! /usr/bin/python3

#Daniel McCarthy 2018
#This class manages the data source threads
#Flask frontend talks to this via xmlRPC class

import paho.mqtt.client as mqttlib
from DriverInterface import Driver as SourceDriver
import json, time, signal, sys

class Manager():
    Sources = {} # type: Dict[str, SourceDriver] #Dict of source objects

    def __init__(self):
        signal.signal(signal.SIGINT, self.exit_gracefully)
        signal.signal(signal.SIGTERM,self.exit_gracefully)
        self.client = mqttlib.Client("Manager")
        self.client.connect("mosquitto")
        # self.client.connect("127.0.0.1")
        self.client.loop_start()

    def exit_gracefully(self, signum, frame):
        print("Manager.py got SIGINT or SIGTERM, Killing all the things now")
        for key, source in self.Sources.items():
            source.Kill()
        sys.exit()


    def DelSource(self, Source):
        pass

    def AddSource(self, Addr, Name, driverName):
        if Name in self.Sources:
            return "Failed, Name already in use"
        else:
            self.Sources[Name] = SourceDriver(Addr, Name, driverName, self.client)
            return "Success"

if __name__ == '__main__':
    Core = Manager()
    #Core.LoadConfig("default.json") #Loads config and Starts sources
    #Core.StartDBIngest()
    #Core.StartWebAPI()
    #Start RPC Server
    Core.AddSource("addr", "Test0", "drv-dummy.py")
    Core.AddSource("addr", "Test1", "drv-dummy.py")
    Core.Sources["Test0"].AddTag("Jerp", 200, "constant")
    Core.Sources["Test0"].AddTag("Derp", 50, "constant")
    Core.Sources["Test1"].AddTag("Herp", 100, "constant")
    time.sleep(5)
    print("Test0 Status: {0}".format(Core.Sources["Test0"].GetStatus()))
    print("Killing Source Processes")
    for key, Source in Core.Sources.items():
        Source.Kill()
    print("Done!")
