#System Logging Process

import paho.mqtt.client as mqttlib
import time

class LoggerProcess():
    def __init__(self, Logfile):
        self.LogChannel = "Log"
        self.Logfile = Logfile
        self.mqttAddr = "mosquitto"
        self.client = mqttlib.Client("LoggerProcess")
        self.client.connect(self.mqttAddr)
        self.client.subscribe(self.LogChannel)
        self.client.on_message=self.LogHandler
        self.client.loop_forever()

    def LogHandler(self, client, userdata, msg):
        print(msg.payload)
        with open(self.Logfile, 'a') as f:
            f.write(msg.payload.decode() + "\n")

class Log():
    def __init__(self, Name, mqttc):
        self.Name = Name #Name of Log source
        self.client = mqttc #MQTT Client
        self.LogChannel = "Log"

    def __init__(self, Name):
        self.Name = Name #Name of Log source
        self.mqttAddr = "mosquitto"
        self.client = mqttlib.Client(Name + "/Log")
        self.client.connect(self.mqttAddr)
        self.LogChannel = "Log"
        self.client.loop_start() #Start loop thread to reply to pings from broker

    def Log(self, Message):
        self.client.publish(self.LogChannel, "[{0}][{1}][{2}]".format(self.Name, time.time(), Message))

if __name__ == "__main__":
    Logger = LoggerProcess('/usr/src/app/config/logger.log')
