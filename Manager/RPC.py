#File contains the xmlRPC implementation.
#We pass it the core and config objects which it calls when RPC's are called
#xmlrpc seems like the simplest way
#Daniel McCarthy 2018

from xmlrpc.server import SimpleXMLRPCServer
from xmlrpc.server import SimpleXMLRPCRequestHandler
import os, subprocess

# Restrict to a particular path.
class RequestHandler(SimpleXMLRPCRequestHandler):
    rpc_paths = ('/RPC2',)

class RPC():
    def __init__(self, Core, Config):
        #We pass RPC function calls to Core and Config
        self.Core = Core
        self.Config = Config
        self.server = SimpleXMLRPCServer(("localhost", 9001), requestHandler=RequestHandler)

        self.server.register_introspection_functions()
        #Drivers
        self.server.register_function(self.Drivers)
        self.server.register_function(self.DriverDef)
        #Sources
        self.server.register_function(self.Sources)
        self.server.register_function(self.SourceStatus)
        self.server.register_function(self.SourceAdd)
        self.server.register_function(self.SourceDel)
        #server.register_function(SourceMod) //Is this needed?
        #Tags
        self.server.register_function(self.TagSet)
        self.server.register_function(self.TagDel)


    #Get a list of the drivers
    def Drivers(self):
        return os.listdir("./SourceDrivers")

    def DriverDef(self, driver):
        result = subprocess.check_output(["/usr/src/app/SourceDrivers/{0}".format(driver)])
        return result

    #Get a list of the sources
    def Sources(self):
        return [*self.Core.Sources]

    #Get the Status of a source
    def SourceStatus(self, Source):
        if Source in self.Core.Sources:
            return self.Core.Sources[Source].GetStatus()
        else:
            return "Name doesn't exist"

    #Add a Source
    def SourceAdd(self, Addr, Name, Driver):
        if Name not in self.Core.Sources:
            return self.Core.AddSource(Addr, Name, Driver)
        else:
            return "Name:Error"

    #Kill & remove a Source
    def SourceDel(self, Name):
        if Name in self.Core.Sources:
            self.Config.SourceDel(Name)
            self.Core.Sources[Name].Kill()
            del self.Core.Sources[Name]
            return "Success"
        else:
            return "Name:Error"

    #Add or Set a tag
    def TagSet(self, SName, Name, Mode, Interval):
        if(Interval >= 10 and (Mode == 'event' or Mode == 'continuous') and SName in self.Core.Sources):
            self.Config.TagSet(SName, Name, Mode, Interval)
            self.Core.Sources[SName].SetTag(Name, Interval, Mode)
            return "Done"
        else:
            return "Something went wrong"

    #Remove a Tag
    def TagDel(self, SName, Name):
        if SName in self.Core.Sources:
            self.Config.TagDel(SName, Name)
            self.Core.Sources[SName].DelTag(Name)
            return "Done"
        else:
            return "Bad Source Name"

    #Start RPC event loop
    def Run(self):
        self.server.serve_forever()

#if __name__ == "__main__":
    # Create server
