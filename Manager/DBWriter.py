#! /usr/bin/python3

# Receives data from MQTT and writes it to influxdb
# Alastair Robertson, Daniel McCarthy - 04/2018

import paho.mqtt.client as mqtt
from influxdb import InfluxDBClient
import json, time
from SysLog import Log

class DBWrite():
    def __init__(self):  #TODO add error checking :P
        self.mqttc = mqtt.Client()
        self.influxAddr = "influxdb"
        self.mqttAddr = "mosquitto"
        self.DBName = "Logger"
        self.CommitThreshold = 500 #Number of messages to wait for until commit
        self.CommitTimeout = 5     #Number of seconds to wait before commiting if we didn't reach message threshold
        self.mqttc.connect(self.mqttAddr)
        self.mqttc.subscribe("Source/#", qos=2)
        self.mqttc.on_message=self.MessageHandler
        self.log = Log("DBWriter")
        self.log.Log("DBWriter UP")
        self.influxc = InfluxDBClient(host=self.influxAddr, port=8086)
        self.CreateDB()
        self.dataBuffer = [] #List of data to commit
        self.timeAtLastCommit = time.time() #Time AT last commit

        #self.mqttc.loop_start() #Start MQTT Event Loop Thread

    #CreateDB if it doesn't exist, switch to it if it does.
    def CreateDB(self):
        self.log.Log("DB Create")
        dblist = self.influxc.get_list_database()
        if self.DBName not in dblist:
            self.influxc.create_database(self.DBName)
        self.influxc.switch_database(self.DBName)

    #Commit the messages we've rx'd
    def Commit(self):
        n = len(self.dataBuffer)
        if (n > 0):
            self.influxc.write_points(self.dataBuffer)
            self.log.Log("Committed {0} to DB ({1}/s)".format(n, n / (time.time() - self.timeAtLastCommit)))
            self.dataBuffer = []
        self.timeAtLastCommit = time.time()

    def MessageHandler(self, client, userdata, msg):
        topic = msg.topic
        message = json.loads(msg.payload.decode("utf-8"))
        data = {
            "measurement": topic,
            "time": message['time'],
            "fields": {
                "value" : message['value']
                }
            }

        #Append dict to buffer
        self.dataBuffer.append(data)

        #Check if we hit threshold
        if len(self.dataBuffer) > self.CommitThreshold: # len is O(1) :D https://wiki.python.org/moin/TimeComplexity
            self.Commit()

    def CheckTime(self):
        self.mqttc.loop()
        if time.time() > (self.timeAtLastCommit + self.CommitTimeout):
            self.Commit()


if __name__ == '__main__':
    Writer = DBWrite()
    while(True): #Anyone got a better way?
        Writer.CheckTime()
        #time.sleep(0.1) # Sleep for 100ms
