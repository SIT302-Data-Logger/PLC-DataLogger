#Data Logger Rest API Implementation
# James Xu, Daniel McCarthy 2017,2018
#Alastair Robertson 05/2018

import flask
from flask import Flask, request, Response, jsonify, make_response
from flask_session import Session
import json, os, base64, redis, sqlite3, xmlrpc.client


app = flask.Flask(__name__)
app.secret_key = 'ashdashd&^SDasddasdgds'
app.config['SESSION_TYPE'] = 'redis'
app.config['SESSION_REDIS'] = redis.StrictRedis(host='redis', port=6379)
Session(app)

try:
    db = sqlite3.connect('/usr/src/app/config/db.sqlite')
except:
    #for testing purposes when docker is not run
    db = sqlite3.connect('/root/Desktop/PLC/PLC-DataLogger/conf/manager/db.sqlite')

def set_session(s):
    flask.session['username'] = s


def get_session(s):
    try:
        return flask.session[s]
    #KeyError
    except:
        return False

def delete_session(s):
    try:
        del flask.session[s]
    except:
        pass

def check_session():
    if get_session('username') == False:
        return True ##TODO Change this back!!!! TODO
    else:
        #return true if user is logged in
        return True


s = xmlrpc.client.ServerProxy('http://localhost:9001') #connect to XMLRPC
print(s.system.listMethods())

#Get DSD list
@app.route("/api/drivers")
def drivers():
    return jsonify(s.Drivers())


#Get Source List
@app.route("/api/sources")
def sources():
    return jsonify(s.Sources())

#Get Source Status, Add source, delete source
@app.route("/api/sources/<source>", methods=['GET', 'POST', 'DELETE'])
def GetSource(source):
    if request.method == 'GET':
        source = s.SourceStatus(source)
        if source == "Name doesn't exist":
            return Response(status=404)
        else:
            return jsonify(source)
    elif request.method == 'POST':
        addr = request.json['Address']
        driver = request.json['DriverName']
        res = s.SourceAdd(addr, source, driver)
        if res == "Name:Error":
            return Response(status=409)
        else:
            return jsonify([source]) #Respond with list
    elif request.method == 'DELETE':
        ret = s.SourceDel(source)
        if ret == "Name:Error":
            return Response(status=404)
        else:
            return jsonify([source])

@app.route("/api/<source>/<tag>", methods=['POST', 'DELETE'])
def SetTag(source, tag):
    ret = ''
    if request.method == 'POST':
        mode = request.json['Mode']
        interval = int(request.json['Interval'])
        ret = s.TagSet(source, tag, mode, interval)
        tagModel = { tag: { 'mode': mode, 'interval': interval } }
    elif request.method == 'DELETE':
        ret = s.TagDel(source, tag)
        return jsonify(tag)
    if ret != "Done":
        return Response(status=400)
    else:
        return jsonify(tagModel)


#TODO hook this up to an auth system
@app.route("/api/auth", methods=['POST', 'GET', 'DELETE'])
def login():
    if request.method == 'GET':
        return '', 200
    elif request.method == 'DELETE':
        delete_session('username')
        return 'Successfully logged out'
    elif request.method == 'POST':
        #username = base64.b64decode(request.form['Username'])
        #password = base64.b64decode(request.form['Password'])
        #credentials for testing purposes
        username = 'admin'
        password = 'pword'
        c = db.cursor()
        find_user = ("SELECT * FROM Users WHERE username = ? AND password = ?")
        c.execute(find_user,[(username),(password)])
        results = c.fetchall()
        #in future add hashed passwords, for now/mvp store plaintext
        if results:
            #login successful
            #store session in redis
            set_session(username)
            #flask.session['username'] = username
            return username
        else:
            return '', 401


@app.errorhandler(404)
def source_not_found (e) :
    return Response(status=404)

@app.errorhandler(405)
def source_exists (e) :
    return Response(status=405)
