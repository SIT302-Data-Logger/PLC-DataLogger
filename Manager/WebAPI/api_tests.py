import os
import LoggerAPI
import unittest

class ApiTestCase(unittest.TestCase):

    def setUp(self):
        self.app = LoggerAPI.app.test_client()



    def login(self):
        return self.app.post('/auth')

    def test_login(self):
        rv = self.login
        assert b'admin' in rv.data



if __name__ == '__main__':

    unittest.main()
