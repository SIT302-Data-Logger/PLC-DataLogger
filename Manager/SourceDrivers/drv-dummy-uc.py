#! /usr/local/bin/python3

#TODO: Use py bytecode compilation for speed
#TODO: Make timing more accurate, implementation is a bit weird but works for now.

#This is an implementation of the data source interface drivfer
#It implements the MQTT interface as well as a data source interface which in this case is random.random()

#This implementation may be used as a basis for future source drivers

#Invoke is <name.py> <mqttIP> <datachannel> <controlChannel> <replyChannel> <Name>
#allows management process to invoke this process and control it.
#This should be the same, regardless of which language the implementation is written in

import paho.mqtt.client as mqttlib
import random, sys, json, time, signal, datetime
from threading import Timer

#Debug print output
def Dprint(str):
    #print("drv-Dummy: {0} ".format(str))
    #for prod
    return

class mqtt:
    data = []

    tagtimes = {}
    tags = {}

    #Initializer
    def __init__(self, srcAddr ,dataChannel, controlChannel, feedbackChannel, ClientName):
        signal.signal(signal.SIGINT, self.exit_gracefully)
        signal.signal(signal.SIGTERM,self.exit_gracefully)
        self.Name = ClientName
        self.dataChannel = dataChannel
        self.controlChannel = controlChannel
        self.feedbackChannel = feedbackChannel
        # self.mqttAddr = "127.0.0.1" #Change if need be
        self.mqttAddr = "mosquitto"
        self.srcAddr = srcAddr
        #Connect to MQTT
        self.client = mqttlib.Client(ClientName)
        self.client.connect(self.mqttAddr)
        self.client.subscribe(self.controlChannel)
        self.client.on_message=self.ControlHandler
        self.GetData()  #Start Getting Data
        self.Log("Up!")
        self.ProcessData()

    def Log(self, Message):
        self.client.publish("Log", "[{0}][{1}][{2}]".format(self.Name, time.time(), Message))

    def exit_gracefully(self, signum, frame):
        sys.exit()

    def GetData(self):
        #print("Thread!") #We cannot print here, it takes too long
        Timer(0.01, self.GetData).start() #0.01
        for key, tagtime in self.tagtimes.items():
            if tagtime <= 0:
                self.tagtimes[key] = self.tags[key]["interval"]
                log = self.GetTag(key)
                self.client.publish(self.dataChannel + "/" + log["tag"], json.dumps(log), qos=0) # Add publish call here to remove race condition
            else:
                self.tagtimes[key] -= 10

    def ProcessData(self):
        self.client.loop() #MQTT Loop in main thread
        Timer(0.001, self.ProcessData).start() #1ms :O

    #Gets data for individual tags, here that just means returning a random number
    def GetTag(self, tag):
        #return tag, tagvalue, time
        var = random.randrange(0,1023)

        
        value = {"tag":tag, "value":var, "time": datetime.datetime.now().isoformat()}
        return value

    def Status(self):
        #Build data structures to send
        Status = {"status":"Okay", "tags":self.tags}
        return json.dumps(Status)

    #Handles Control Messages
    def ControlHandler(self, client, userdata, message):
        msg = json.loads(bytes.decode(message.payload))
        #GetStatus
        if(msg["type"] == "GetStatus"):
            Dprint("Got message!")
            self.client.publish(self.feedbackChannel, self.Status())

        #SetTag, Making assumption that all fields are as expected
        elif(msg["type"] == "SetTag"):
            self.Log("SetTag")
            self.tags[msg["tag"]] = {"mode":msg["mode"], "interval":msg["interval"]} #Interval measured in tens of ms
            self.tagtimes[msg["tag"]] = msg["interval"]
            Dprint("Got SetTag message!: {0}".format(json.dumps(self.tags)))

        #DelTag
        elif(msg["type"] == "DelTag"):
            self.Log("DelTag")
            tagToDel = msg["tag"]
            if(tagToDel in self.tags.keys()):
                del self.tags[tagToDel]
                del self.tagtimes[tagToDel]
                Dprint("Deleted tag: {0}".format(tagToDel))
            else:
                Dprint("Tag \"{0}\" does not exist!".format(tagToDel))
            Dprint("Got DelTag ({0}) message!".format(msg["tag"]))

if(__name__ == '__main__'):
    #print(sys.argv)
    if len(sys.argv) == 6: #Invoked from Manager
        main = mqtt(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5]) #All we need to call is the Initializer
        print("UP")
    elif len(sys.argv) <= 6: #Invoked to print definition
        definition = {"IntervalMin" : 10, "AddressType" : "None"} #Define
        print(json.dumps(definition))
        sys.exit()
    else:
        print("Not enough args!")
        sys.exit()
    while True: #Sleep forever
        time.sleep(10)
        pass
        #main.ProcessData() #Main event loop
