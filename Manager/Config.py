#! /usr/bin/python3

#This file contains code to manage the configuration file

class Config():
    def __init__(self, ConfigFileName):
        self.FileName = ConfigFileName #Name of the configfile we write/read
        self.Config = ''#readconfig

    def SourceAdd(self, Address, SourceName, DriverName): #Called whenever we add a data source to the system
        print("Added Source: {0}".format(SourceName))

    def SourceDel(self, SourceName): #Called whenever we delete a data source from the system
        print("Removed Source: {0}".format(SourceName))

    def TagSet(self, SourceName, TagName, Mode, Interval):  #Called whenever we add or modify a Tag
        print("Added Tag: {0}/{1}".format(SourceName, TagName))

    def TagDel(self, SourceName, TagName): #Called whenever we remove a Tag
        print("Removed Tag: {0}/{1}".format(SourceName, TagName))

    def Set(self, Manager):
        print("Set from config file")
        #e.g:
        #for source in sources
        #Manager.AddSource()
        #Manager.AddTag()
        #And so on

if __name__ == "__main__":
    #Test Code here
    print("Hello World")
