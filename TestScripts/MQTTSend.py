#! /usr/bin/python3

#For testing if MQTT is actually working

import sys, random, subprocess, json, time
import paho.mqtt.client as mqttlib

#Connect to MQTT

print("MQTT Connect")
client = mqttlib.Client("Test{0}".format(random.randint(0, 10000)))
client.connect("127.0.0.1")

client.publish("Derp", "Herp")
