#! /usr/bin/python3

#Testing happens here, These methods could also be used as a starting point in the management process
#TODO: Change to Allow non python programs to be invoked

import sys, random, subprocess, json, time
import paho.mqtt.client as mqttlib

#Connect to MQTT

def Dprint(str):
    print("Test: {0}".format(str))

def on_message(rclient, userdata, message):
    print(message.payload)
    msg = json.loads(bytes.decode(message.payload))
    if(msg["status"] == "Okay"):
        print("Got message back!")


print("MQTT Connect")
client = mqttlib.Client("Test{0}".format(random.randint(0, 10000)))
client.connect("127.0.0.1")
client.subscribe("GlobalResp")
client.on_message = on_message


def StartDriver(name):
    print(name)
    return subprocess.Popen(["python3","{0}".format(name), "127.0.0.1", "TestData", "TestCtrl", "GlobalResp", "DRV-Test"])

def GetStatus():
    msg = {"type" : "GetStatus"}
    client.publish("TestCtrl", json.dumps(msg))

def SetTag():
    msg = {"type":"SetTag", "interval":20, "tag":"TestTag", "mode":"constant"}
    client.publish("TestCtrl", json.dumps(msg))

def DelTag():
    msg = {"type":"DelTag", "tag":"TestTag"}
    client.publish("TestCtrl", json.dumps(msg))

def init():
    if len(sys.argv) == 2:
        #Start Process
        print("Launching Driver Process")
        p = StartDriver(sys.argv[1])
        time.sleep(1) #wait for process to connect
        #GetStatus
        print("Attempting to get Process status")
        Status = GetStatus()
        time.sleep(1) #wait for process to send response

        #SetTag
        SetTag()
        GetStatus()
        time.sleep(3) #wait
        DelTag()
        time.sleep(1) #wait
        SetTag()
        time.sleep(3) #wait
        GetStatus()
        DelTag()
        time.sleep(1) #wait

        #Messages should be visible in console
        print("Kill Process")
        p.kill()

    else:
        print("No driver name given")


if __name__ == '__main__':
    init()
