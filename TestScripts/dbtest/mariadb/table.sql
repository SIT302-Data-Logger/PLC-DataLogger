create table test (
	ID int NOT NULL,
	Tag varchar(64) NOT NULL,
	Value DOUBLE,
	Time DATETIME NOT NULL,
	PRIMARY KEY (ID)
);

create table test_idx (
	ID int NOT NULL,
	Tag varchar(64) NOT NULL,
	Value DOUBLE,
	Time DATETIME NOT NULL,
	PRIMARY KEY (ID)
);

create index idx_tt
on test_idx (Tag, Time);
