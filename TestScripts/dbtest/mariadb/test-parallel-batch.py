#! /usr/bin/python3
import mysql.connector as mariadb
import time, math
import multiprocessing

#cursor.execute("DROP TABLE test")
#cursor.execute("DROP TABLE test_idx")
#testdb.commit()

#cursor.execute("create table test ( ID int NOT NULL AUTO_INCREMENT, Tag varchar(64) NOT NULL, Value DOUBLE, Time DATETIME NOT NULL, PRIMARY KEY (ID) )")
#cursor.execute("create table test_idx ( ID int NOT NULL AUTO_INCREMENT, Tag varchar(64) NOT NULL, Value DOUBLE, Time DATETIME NOT NULL, PRIMARY KEY (ID) )")
#cursor.execute("create index idx_tt on test_idx (Tag, Time)")
#testdb.commit()

def dbwrite():
    N = 100000
    testdb = mariadb.connect(user='test', password='', database='test')
    cursor = testdb.cursor()
    t0 = time.time()
    # ID, Tag, Value, Time
    print("Start")
    for i in range(0, N):
        #print("INSERT INTO test VALUES ({0}, {1}, {2}, {3})".format(i,"Derp",i/math.pi,time.time()))
        cursor.execute("INSERT INTO test (Tag, Value, Time) VALUES ({0}, {1}, {2})".format("\"Derp\"",i/math.pi,time.time()))
    testdb.commit()
    t1 = time.time()
    elapsed = t1-t0
    print("Took {0} Seconds, {1}/Second".format(elapsed, (N/elapsed)))

    print("Start IDX DB")
    for i in range(0, N):
        #print("INSERT INTO test VALUES ({0}, {1}, {2}, {3})".format(i,"Derp",i/math.pi,time.time()))
        cursor.execute("INSERT INTO test_idx (Tag, Value, Time) VALUES ({0}, {1}, {2})".format("\"Derp\"",i/math.pi,time.time()))
    testdb.commit()
    t1 = time.time()
    elapsed = t1-t0
    print("Took {0} Seconds, {1}/Second".format(elapsed, (N/elapsed)))

#dbwrite()

jobs = []
for i in range(4):
    p = multiprocessing.Process(target=dbwrite)
    jobs.append(p)
    p.start()
