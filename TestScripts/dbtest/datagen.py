#! /usr/bin/python3

import paho.mqtt.client as mqtt
import random, time, json

client = mqtt.Client("Source{0}".format(random.randint(0,10000)))
client.connect("127.0.0.1")

while True:
    client.publish("Random", json.dumps((random.random(), "Derp", random.random(), "Herp")));
#    time.sleep(0.000001);
