#! /usr/bin/python3

from influxdb import InfluxDBClient as influxdb
import time, random
from datetime import datetime
import multiprocessing
import os

def dbwrite():
    N = 100000
    client = influxdb('localhost', 8086, 'root', 'root', 'test')

    t0 = time.time()
    json_body = []
    for i in range(0,N):
        json_body.append(
                {
                    "measurement": "PLC1",
                    "time": format(str(datetime.now())),
                    "fields": {
                        "value": random.random()
                        }
                    }
                )
    client.write_points(json_body)

    t1 = time.time()
    elapsed = t1-t0
    print("T={0}, {1}/S".format(elapsed, N/elapsed))
    return

jobs = []
for i in range(4):
    p = multiprocessing.Process(target=dbwrite)
    jobs.append(p)
    p.start()
    
