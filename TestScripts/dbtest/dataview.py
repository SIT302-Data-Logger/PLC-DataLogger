#! /usr/bin/python3

import paho.mqtt.client as mqtt
import random, time

client = mqtt.Client("Viewer{0}".format(random.randint(0,1000)))
client.connect("127.0.0.1")

client.subscribe("#")

messages = 0

t = time.time()

def on_message(client, userdata, message):
    global messages
    messages = messages + 1

client.on_message=on_message

while True:
    client.loop()
    if time.time() > t + 1:
        print("Messages/S {0}".format(messages))
        t = time.time()
        messages = 0
