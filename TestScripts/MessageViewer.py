#! /usr/bin/python3

import random, time
import paho.mqtt.client as mqtt

client = mqtt.Client("Viewer{0}".format(random.randint(0,1000)))
client.connect("172.18.0.2")

client.subscribe("#") #sub all channels

def on_message(client, userdata, message):
    print("{0}: {1}".format(message.topic, message.payload))

client.on_message=on_message

while True:
    client.loop()
