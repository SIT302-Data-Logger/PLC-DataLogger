# Test Scripts
This folder contains scripts to test various functionality of the system.

## DriverTest.py
Tests source driver.

## DupeFinder.py
Finds sequential duplicate messages on the MQTT bus.

## MessageViewer.py
View Messages on the MQTT bus as they are sent.
