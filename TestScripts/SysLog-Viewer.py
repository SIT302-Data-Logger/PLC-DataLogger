#System Logging Process

import paho.mqtt.client as mqttlib
import time

class LoggerProcess():
    def __init__(self):
        self.LogChannel = "Log"
        self.mqttAddr = "172.18.0.3"
        self.client = mqttlib.Client("LoggerTest")
        self.client.connect(self.mqttAddr)
        self.client.subscribe(self.LogChannel)
        self.client.on_message=self.LogHandler
        self.client.loop_forever()

    def LogHandler(self, client, userdata, msg):
        print(msg.payload)

if __name__ == "__main__":
    Logger = LoggerProcess()
