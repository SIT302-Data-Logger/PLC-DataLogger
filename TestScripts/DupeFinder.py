#! /usr/bin/python3

import random, time
import paho.mqtt.client as mqtt

client = mqtt.Client("Viewer{0}".format(random.randint(0,1000)))
client.connect("172.18.0.2")

client.subscribe("#", qos=2) #sub all channels

prev_msg = ""

def on_message(client, userdata, message):
    global prev_msg
    if(message.payload == prev_msg):
        print("Dupe: {0}".format(message.payload))
    prev_msg = message.payload
   # print("{0}: {1}".format(message.topic, message.payload))

client.on_message=on_message

while True:
    client.loop()
