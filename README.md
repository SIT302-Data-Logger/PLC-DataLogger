# Data Logger
This is a capstone project being developed as part of SIT374, project Design.
The goal of this project is to develop a Data Logger application for logging data. In this case, the data is from industrial automation systems (PLC's and Microcontrollers). The aim is to also provide an interface by which the data can be visualised and the means to change how and what data is recorded.

## Installation

Assuming Git is installed and you have setup an ssh key:  
`git clone git@gitlab.com:SIT302-Data-Logger/PLC-DataLogger.git`

For those who need to change to the new repository, run:
`git remote set-url origin git@gitlab.com:SIT302-Data-Logger/PLC-DataLogger.git`

### Dependancies
We're using docker-compose to create a container set for the Logger system, you'll need to install docker-ce and docker-compose in order to run it.
Instructions for installation of docker-ce are [here](https://docs.docker.com/install/linux/docker-ce/debian/).
To install docker-compose (on debian) , use `sudo apt-get install docker-compose`, assuming you followed the instructions to add the docker repository from the link above.

## Running the Data Logger
First, run `sh permissions.sh` to create/apply permissions to the persistent folders.

Then run `docker-compose up` to build and run the development version.
  * `Ctrl+C` to kill.
  * To restart the api (for development purposes) `docker-compose restart api`

To use grafana, head to [http://127.0.0.1:8080/grafana/login](http://127.0.0.1:8080/grafana/login)
username/password : `admin/admin`
Add the influxdb data source `http://influxdb:8086` dbname = `Logger`. Sources can be easily added to dashboards, checkout the guide [here](http://docs.grafana.org/guides/getting_started/).

## Test Scripts
**Some of these will no longer work, they need to be modified for Docker Compose**  
There are a number of testing and debugging scripts:  
  - DriverTest, tests conformance of a SourceDriver to the spec  
To run: `DriverTest.py ../SourceDrivers/drv-dummy.py` where the argument is the path to the driver we want to test.
  - MessageViewer, prints all MQTT messages to the shell when run.

## Components
#### WebAPI
API for interfacing to the manager and the database from the Web Interface.  
This resides in the `Manager/WebAPI` folder.  

Add Source: `curl 'http://localhost:8080/api/sources/PLC2' -H 'Content-Type: application/json' --data-binary '{"DriverName":"drv-dummy.py","Address":"127.0.0.1"}'`  
Delete Source: `curl -v -X DELETE http://127.0.0.1:8080/api/sources/PLC2`  
Add Tag: `curl 'http://localhost:8080/api/PLC2/tag1' -H 'Content-Type: application/json' --data-binary '{"Mode":"continuous","Interval":"100"}'`  
Delete Tag: `curl -X DELETE -v http://127.0.0.1:8080/api/PLC2/tag1`  
Auth, send u/p: `curl -X POST -v -d "Username=ZGVycAo=&Password=ZGVycAo=" http://127.0.0.1:8080/api/auth` (Needs some work)

#### WebSockets
Websockets endpoint is `/api/ws`  
It forwards directly to the websockets server running as part of Mosquitto on TCP:1884  
use [MQTT.js](https://github.com/mqttjs/MQTT.js) to access it from the frontend.  

#### Web Interface
User facing interface to the system, provides facilities to view data and modify what and how data is collected.  
Contained in the `ui` folder.
#### Source Drivers
Data source drivers, conform to the MQTT/Logging interface spec and fetch data from data sources at the command of the Manager.  
Drivers reside in `Manager/SourceDrivers`. New drivers which conform with the spec can be copied there and they should *just* work.
#### Manager
Management process, this is the root process, it instantiates everything.  
Lives in `Manager` folder.
#### Database Writer
This process is devoted to writing batches of data to InfluxDB, based on the MQTT/Logging spec. It subscribes to the data source channels, determines which data goes where in the database. Inside `DBProcess` folder.
